<?php

/**
 * @package     Joomla.Site
 * @subpackage  mod_bitrix24
 *
 * @copyright   (C) 2005 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Helper\ModuleHelper;

$buttonId = trim($params->get('button_id', ''));

require ModuleHelper::getLayoutPath('mod_bitrix24', $params->get('layout', 'default'));
